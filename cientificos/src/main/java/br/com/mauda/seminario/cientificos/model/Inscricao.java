package br.com.mauda.seminario.cientificos.model;

import br.com.mauda.seminario.cientificos.model.enums.SituacaoInscricaoEnum;

public class Inscricao {

    private Long id;
    private Boolean direitoMaterial;

    private SituacaoInscricaoEnum situacaoInscricaoEnum = SituacaoInscricaoEnum.DISPONIVEL;
    private Estudante estudante;
    private Seminario seminario;

    public Inscricao(Seminario seminario) {
        this.seminario = seminario;
        this.seminario.getInscricoes().add(this);
    }

    public void comprar(Estudante estudante, Boolean direitoMaterial) {
        this.estudante = estudante;
        this.estudante.getInscricoes().add(this);
        this.direitoMaterial = direitoMaterial;
        this.situacaoInscricaoEnum = SituacaoInscricaoEnum.COMPRADO;
    }

    public void realizarCheckIn() {
        this.situacaoInscricaoEnum = SituacaoInscricaoEnum.CHECKIN;
    }

    public SituacaoInscricaoEnum getSituacao() {
        return this.situacaoInscricaoEnum;
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Boolean getDireitoMaterial() {
        return this.direitoMaterial;
    }

    public void setDireitoMaterial(Boolean direitoMaterial) {
        this.direitoMaterial = direitoMaterial;
    }

    public Estudante getEstudante() {
        return this.estudante;
    }

    public Seminario getSeminario() {
        return this.seminario;
    }
}